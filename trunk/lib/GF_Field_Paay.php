<?php

if (!class_exists('GFForms')){
    die();
}

class GF_Field_Paay extends GF_Field
{
    public $type = 'paay';

    private $is_form_editor;

    public function get_form_editor_field_title()
    {
        return esc_attr__('PAAY Wallet', 'gravityformspaay');
    }

    function get_form_editor_field_settings()
    {
        return array(
            'label_setting',
            'sub_labels_setting',
            'css_class_setting',
            'force_ssl_field_setting',
        );
    }

    public function get_field_label($force_frontend_label, $value)
    {
        $field_label = $force_frontend_label ? $this->label : GFCommon::get_label($this);

        return preg_replace('/PAAY/','<span>PAAY</span>',$field_label);
    }

    public function get_field_content($value, $force_frontend_label, $form)
    {
        $field_label = $this->get_field_label($force_frontend_label, $value);

        $is_form_editor  = $this->is_form_editor();
        $is_entry_detail = $this->is_entry_detail();
        $is_admin        = $is_form_editor || $is_entry_detail;

        $required_div = $is_admin || $this->isRequired ? sprintf("<span class='gfield_required standalone_label'>%s</span>", $this->isRequired ? '*' : '' ) : '';

        $admin_buttons = $this->get_admin_buttons();

        $field_content = sprintf("%s{FIELD}<div class='paay_clear'></div><label class='gfield_label standalone_label' >%s%s</label></div>", $admin_buttons, $field_label, $required_div);

        return $field_content;
    }

    public function get_field_input($form, $value = '', $entry = null)
    {
        if($this->is_entry_detail()){
            return '<div class="ginput_container">PAAY Credit Card fields is not editable</div>';
        }

        $this->is_form_editor  = $this->is_form_editor();

        if($this->is_form_editor){
            return '<br><div class="paay-button-box">
                    <div class="paay-input">
                        <div class="paay-text-line">
                            <div class="paay-text">Checkout with PAAY | What is PAAY?</div>
                            <div class="paay-hint">
                                <i class="paay-icon-question"></i>
                            </div>
                        </div>
                        <div class="paay-button-line">
                            <input class="paay-phone" type="text" placeholder="Mobile number">
                            <button class="paay-button" type="button">PAAY</button>
                        </div>
                    </div>
                </div>';
        }

        if(GFPaay::feedIsEnabled($form['id'])) {
            $buttonScript = file_get_contents(dirname(__FILE__) . '/../templates/paay_gf_scripts.php');
            $button_field = '<div class="paay-button-placeholder"></div>' . $buttonScript;
        }

        return $button_field;
    }
}
