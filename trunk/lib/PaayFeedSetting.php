<?php

class PaayFeedSetting
{
    public $isAmount = false;
    public $amount;

    public $isEmail = false;
    public $email;

    public $isBillingAddress = false;
    public $billingAddress = array();

    private $pre = 'billingInformation_';
    const AMMOUNT_NAME = 'amount';
    const EMAIL_NAME   = 'email';

    const FIRST_NAME = 'billingFirstName';
    const LAST_NAME  = 'billingLastName';
    const ADDRESS1   = 'billingAddress1';
    const ADDRESS2   = 'billingAddress2';
    const CITY       = 'billingCity';
    const POSTCODE   = 'billingPostcode';
    const STATE      = 'billingState';
    const COUNTRY    = 'billingCountry';

    public function __construct($feed)
    {
        if(!array_key_exists('meta', $feed)){
            throw new \Exception('Feed settings does not exist');
        }

        $this->setupAmount($feed['meta']);
        $this->setupEmail($feed['meta']);
        $this->setupBillingAddress($feed['meta']);
    }

    public function setupAmount($meta)
    {
        $value = $meta[self::AMMOUNT_NAME];

        if(strlen($value) > 0){
            $this->amount = $value;
            $this->isAmount = true;
        }
    }

    public function setupEmail($meta)
    {
        $key = $this->pre . self::EMAIL_NAME;
        $value = $meta[$key];

        if(strlen($value) > 0){
            $this->email = $value;
            $this->isEmail = true;
        }
    }

    public function setupBillingAddress($meta)
    {
        foreach($meta as $key => $value){
            if($this->clearBillingKey($key) === self::EMAIL_NAME){
                continue;
            }
            if(!$this->isbillingField($key)){
                continue;
            }
            if(strlen($value) < 1){
                continue;
            }

            $billingKey = $this->clearBillingKey($key);
            $this->billingAddress[$billingKey] = $value;
            $this->isBillingAddress = true;
        }
    }
    
    /**
     * Remove billing part from keys
     * @param string $key
     * @return string
     */
    private function clearBillingKey($key)
    {
        return preg_replace('/^billingInformation\_/','',$key);
    }

    /**
     * Check field is "billing" type
     * @param string $key
     * @return bool
     */
    private function isbillingField($key)
    {
        $match = preg_match('/^billingInformation\_/', $key);
        return (bool)($match === 1);
    }

    public static function feedFields()
    {
        return array(
            array(
                'title'  => 'PAAY Feed Settings',
                'fields' => array(
                    array(
                        'name'    => 'transactionType',
                        'type'    => 'hidden',
                        'default_value' => 'donation',
                    ),
                    array(
                        'name'    => 'paymentAmount',
                        'type'    => 'hidden',
                        'default_value' => 'form_total',
                    ),
                    array(
                        'name' => 'feedName',
                        'label' => 'Name',
                        'type' => 'text',
                        'class' => 'medium',
                        'required' => 1,
                        'tooltip' => '<h6>Name</h6> Enter a feed name to uniquely identify this setup.'
                    ),
                    array(
                        'label'   => 'Total Cost',
                        'type'    => 'radio',
                        'horizontal' => true,
                        'name'    => self::AMMOUNT_NAME,
                        'tooltip' => '<h6>Total Cost</h6>This field indicates the total transaction cost',
                        'choices' => array(
                            array(
                                'label' => '(Pricing Fields) Total',
                                'value'  => 'total',
                            ),
                        ),
                        'default_value' => 'total'
                    ),
                    array(
                        'name' => 'billingInformation',
                        'label' => 'Billing Information',
                        'type' => 'field_map',
                        'tooltip' => '<h6>Billing Information</h6>Map your Form Fields to the available listed fields.',
                        'field_map' => array(
                            array(
                                'name' => self::EMAIL_NAME,
                                'label' => 'Email',
                                'required' => true
                            ),
                            array(
                                'name' => self::FIRST_NAME,
                                'label' => 'First name',
                                'required' => false
                            ),
                            array(
                                'name' => self::LAST_NAME,
                                'label' => 'Last name',
                                'required' => false
                            ),
                            array(
                                'name' => self::ADDRESS1,
                                'label' => 'Address',
                                'required' => false
                            ),
                            array(
                                'name' => self::ADDRESS2,
                                'label' => 'Address 2',
                                'required' => false
                            ),
                            array(
                                'name' => self::CITY,
                                'label' => 'City',
                                'required' => false
                            ),
                            array(
                                'name' => self::POSTCODE,
                                'label' => 'Zip',
                                'required' => false
                            ),
                            array(
                                'name' => self::STATE,
                                'label' => 'State',
                                'required' => false
                            ),
                            array(
                                'name' => self::COUNTRY,
                                'label' => 'Country',
                                'required' => false
                            )
                        )
                    )
                )
            )
        );
    }
}
