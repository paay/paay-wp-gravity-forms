<?php

use PAAY\Plugin\Helpers\SettingsInterface;
use PAAY\Plugin\Helpers\DataExcavatorInterface;

class Excavator implements DataExcavatorInterface
{
    private $settings;
    private $order;

    private $defaultDetails = array(
        'name' => 'Donation',
        'cost' => 0.0
    );

    public function setOrder(GFOrder $order)
    {
        $this->order = $order;

        return $this;
    }

    public function setSettings(SettingsInterface $settings)
    {
        $this->settings = $settings;

        return $this;
    }

    public function excavateBasicParameters()
    {
        $orderId = $this->order->getOrderId();
        
        return array(
            'amount'    => $this->order->getAmount(),
            'orderId'   => $orderId,
            'details'   => $this->details($this->order->getAmount()),
            'returnUrl' => $this->settings->returnUrl(),
            'cancelUrl' => $this->settings->cancelUrl(),
            'statusUrl' => $this->settings->statusUrl($orderId),
            'email'     => $this->order->getEmail(),
            'threeds_visibility' => $this->settings->iframeOption()
        );
    }

    private function details($amount)
    {
        $this->defaultDetails['cost'] = $amount;

        return json_encode(array($this->defaultDetails));
    }

    public function excavateBillingParameters()
    {
        return array(
            'billingFirstName'  => $this->order->getFirstName(),
            'billingLastName'   => $this->order->getLastName(),
            'billingEmail'      => $this->order->getEmail(),
            'billingAddress1'   => $this->order->getAddress1(),
            'billingAddress2'   => $this->order->getAddress2(),
            'billingCity'       => $this->order->getCity(),
            'billingPostcode'   => $this->order->getPostCode(),
            'billingState'      => $this->order->getState(),
            'billingCountry'    => $this->order->getCountry(),
        );
    }

    public function excavateShippingParameters()
    {
        return array();
    }
}
