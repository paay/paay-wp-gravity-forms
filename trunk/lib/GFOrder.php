<?php

/**
 * Class GFOrder
 *
 *  $entry = array(
 *      'id' => 'orderId',
 *      ...
 *      '2'  => 'field_id'
 *      ...
 *      '3.3' => 'sub_field_id'
 *  );
 *
 *  ###################
 *
 *  $form = array(
 *      ...
 *      'fields' => array(
 *          0 => array(
 *              'type' => 'field_type',
 *              'id'   => 'field_id',
 *              ...
 *              'inputs' => array(
 *                  0 => array(
 *                      'id' => 'sub_field_id'
 *                  ),
 *                  1 => array(...),
 *                  ...
 *              )
 *          ),
 *          1 => array(...)
 *          ...
 *      ),
 *      ...
 *  );
 *
 *  ###################
 *
 *  $feedSettings->billingAddress = array(
 *      'feed_key_name' => 'id_field' OR 'sub_field_id'
 *  );
 */

class GFOrder
{
    private $entry;
    private $form;
    private $fields;
    private $feedSettings;
    private $orderId;

    private $firstNameFieldType = 'name';
    private $firstNameIdSubField = '3';

    private $lastNameFieldType = 'name';
    private $lastNameIdSubField = '6';

    private $address1FieldType = 'address';
    private $address1IdSubField = '1';

    private $address2FieldType = 'address';
    private $address2IdSubField = '2';

    private $cityFieldType = 'address';
    private $cityIdSubField = '3';

    private $postCodeFieldType = 'address';
    private $postCodeIdSubField = '5';

    private $stateFieldType = 'address';
    private $stateIdSubField = '4';

    private $countryFieldType = 'address';
    private $countryIdSubField = '6';

    public function __construct($entry, $form, PaayFeedSetting $feedSettings)
    {
        $this->entry = $entry;
        $this->orderId = $entry['id'];

        $this->form = $form;
        $this->fields = $form['fields'];

        $this->feedSettings = $feedSettings;
    }

    public function getAmount()
    {
        if(!$this->feedSettings->isAmount){
            return 0.0;
        }
        $type = $this->feedSettings->amount;
        $field = current(GFAPI::get_fields_by_type($this->form, array($type), true));

        if(!$field || !array_key_exists('id', $field)){
            return 0.0;
        }

        $key = $field['id'];
        if(!array_key_exists($key, $this->entry)){
            return 0.0;
        }

        return $this->entry[$key];
    }

    public function getOrderId()
    {
        return $this->orderId;    
    }
    
    public function getEmail()
    {
        if($this->feedSettings->isEmail){
            $key = $this->feedSettings->email;
            if(array_key_exists($key, $this->entry)){
                return $this->entry[$key];
            }
        }

        return null;
    }

    public function getFirstName()
    {
        return $this->getBillingField(
            PaayFeedSetting::FIRST_NAME,
            $this->firstNameFieldType,
            $this->firstNameIdSubField
        );
    }
    
    public function getLastName()
    {
        return $this->getBillingField(
            PaayFeedSetting::LAST_NAME,
            $this->lastNameFieldType,
            $this->lastNameIdSubField
        );
    }

    public function getAddress1()
    {
        return $this->getBillingField(
            PaayFeedSetting::ADDRESS1,
            $this->address1FieldType,
            $this->address1IdSubField
        );
    }

    public function getAddress2()
    {
        return $this->getBillingField(
            PaayFeedSetting::ADDRESS2,
            $this->address2FieldType,
            $this->address2IdSubField
        );
    }

    public function getCity()
    {
        return $this->getBillingField(
            PaayFeedSetting::CITY,
            $this->cityFieldType,
            $this->cityIdSubField
        );
    }

    public function getPostCode()
    {
        return $this->getBillingField(
            PaayFeedSetting::POSTCODE,
            $this->postCodeFieldType,
            $this->postCodeIdSubField
        );
    }

    public function getState()
    {
        return $this->getBillingField(
            PaayFeedSetting::STATE,
            $this->stateFieldType,
            $this->stateIdSubField
        );
    }

    public function getCountry()
    {
        return $this->getBillingField(
            PaayFeedSetting::COUNTRY,
            $this->countryFieldType,
            $this->countryIdSubField
        );
    }

    private function getBillingField($feedKey, $fieldType, $subFieldId = null)
    {
        # if field is mapped then get id field from feed settings
        # and return entry value if exist
        if($this->isMapped($feedKey)){
            return $this->mappedField($feedKey);
        }

        $field = current(GFAPI::get_fields_by_type($this->form, array($fieldType), true));

        if($subFieldId !== null) {
            return $this->subfieldValue($field, $subFieldId);
        }

        if($value = $this->notMappedValue($field)){
            return $value;
        }
        
        return null;
    }
    
    private function mappedField($feedKey)
    {
        $entryId = $this->feedSettings->billingAddress[$feedKey];
        
        if($value = $this->entry($entryId)){
            return $value;
        }
        
        return null;
    }

    private function isMapped($key)
    {
        if(!$this->feedSettings->isBillingAddress){
            return false;
        }

        if(!array_key_exists($key, $this->feedSettings->billingAddress)){
            return false;
        }

        return true;
    }

    private function subfieldValue($field, $subId)
    {
        if(!$field || !array_key_exists('id', $field)){
            return null;
        }

        if(!array_key_exists('inputs', $field)){
            return null;
        }

        $subId = $field['id'] .'.'.$subId;
        foreach($field['inputs'] as $subField){
            if($subField['id'] == $subId && $value = $this->entry($subId)){
                return $value;
            }
        }

        return null;
    }
    
    private function notMappedValue($field)
    {
        if(!$field || !array_key_exists('id', $field)){
            return false;
        }

        if($value = $this->entry($field['id'])){
            return $value;
        }
        
        return false;
    }

    private function entry($key)
    {
        if(!array_key_exists($key, $this->entry)){
            return false;
        }

        return $this->entry[$key];
    }
}
