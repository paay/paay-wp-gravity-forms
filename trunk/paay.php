<?php
/*
Plugin Name: Gravity Forms PAAY Standard Add-On
Plugin URI: http://www.gravityforms.com
Description: Integrates Gravity Forms with PAAY Payments Standard.
Version: 2.4.5
Author: PAAY
Author URI: https://www.paay.co/
Text Domain: gravityformspaay
*/

define('GF_PAAY_VERSION', '2.4.5');

require_once('paay_wallet_lib/DataHelper.php');
require_once('paay_wallet_lib/Client.php');

add_action('init', 'paay_gf_handler');
add_action('gform_loaded', array('GF_Paay_Bootstrap', 'load'), 5);

class GF_Paay_Bootstrap
{
    public static function load()
    {
        if (!method_exists('GFForms', 'include_payment_addon_framework')) {
            return;
        }

        require_once('class-gf-paay.php');
        GFAddOn::register('GFPaay');
    }
}

# WALLET FUNCTIONALITY

function paay_gf_settings()
{
    static $settings = null;

    if (null === $settings) {
        $settings = get_option('gravityformsaddon_gravityformspaay_settings');
    }

    return $settings;
}

function paay_gf_api()
{
    static $api = null;

    if (null === $api) {
        $settings = paay_gf_settings();
        $api = new Paay_API_Client(
            rtrim($settings['host-wallet'], '/'),
            $settings['key'],
            $settings['secret']
        );
    }

    return $api;
}

function gf_paay()
{
    return GFPaay::get_instance();
}

function setDataByOrderId($order_id, array $data)
{
    file_put_contents('/tmp/data-'.$order_id, json_encode($data));
}

function getDataByOrderId($order_id)
{
    return json_decode(file_get_contents('/tmp/data-'.$order_id), true);
}

function setTransactionByOrderId($order_id, $transaction_id)
{
    file_put_contents('/tmp/transaction-'.$order_id, $transaction_id);
}

function getTransactionByOrderId($order_id)
{
    return file_get_contents('/tmp/transaction-'.$order_id);
}

function isApproved(\stdClass $response)
{
    $response = $response->response;
    if (('success' !== strtolower($response->message)) ||
        (!isset($response->data->Transaction->signature)) ||
        ('approved' !== strtolower($response->data->Transaction->state))
    ) {
        return false;
    }

    return true;
}

function paay_gf_handler()
{
    $available_modules = array(
        'createTransactionGF',
        'cancelTransactionGF',
        'awaitingApprovalGF',
        'sendWebAppLinkGF',
    );

    if(!array_key_exists('paay-module', $_GET)){
        return;
    }
    $module = trim($_GET['paay-module']);

    if (!in_array($module, $available_modules)) {
        return;
    }

    $module = 'paay_'.$module.'Handler';
    $response = $module();
    header('Content-Type: application/javascript');
    echo $response;
    exit;
}

function paay_gf_order_total(array $order)
{
    $dataHelper = new DataHelper();
    $idForm = $order['form']['id'];

    $dataHelper->setFeedData(current(GFAPI::get_feeds(null, $idForm, 'gravityformspaay')))
        ->setForm($order['form'])
        ->setPost($order['post']);

    return $dataHelper->getTotalFromPost();
}

function paay_createTransactionGFHandler()
{
    $order_id = $_GET['order_id'];
    if (empty($order_id)) {
        return;
    }
    $order = getDataByOrderId($order_id);
    $total = paay_gf_order_total($order);
    $return_url = $order['entry']['source_url'];
    $customer = paay_gf_api()->getCustomerByPhone($_GET['telephone']);

    $items = array(
        array(
            'description'   => 'Donation',
            'quantity'      => 1,
            'unit_price'    => $total,
        ),
    );
    $customer->Address = array($customer->Address[0]);
    $shippings = array(
        'No shipping' => array(
            'price'         => 0,
            'tax_percent'   => 0,
        ),
    );

    try {
        $result = paay_gf_api()->createTransaction($order_id, $customer, $items, $shippings, $return_url);
        setTransactionByOrderId($order_id, $result->response->data->Transaction->id);
        $result->response->order_id = $order_id;
        $response = 'paay.api.createTransactionCallback('.json_encode($result).')';

        return $response;
    } catch (\Exception $e) {
        return 'paay.api.error("'.$e->getMessage().'")';
    }
}

function paay_awaitingApprovalGFHandler()
{
    global $wpdb;
    $lead_meta = GFFormsModel::get_lead_meta_table_name();

    $order_id = $_GET['order_id'];
    $transaction_id = getTransactionByOrderId($order_id);
    $order = getDataByOrderId($order_id);
    $response = paay_gf_api()->getTransactionStatus($transaction_id);
    $return_url = $order['entry']['source_url'];

    if (isApproved($response)) {
        $donorData = json_decode($response->response->data->Transaction->customer);
        $donor = $donorData->first_name.' '.$donorData->last_name;

        $entryId = $order['idE'];

        $wpdb->update($lead_meta, array('meta_value' => $donor), array('lead_id' => $entryId, 'meta_key' => 'donor'));
        $wpdb->update($lead_meta, array('meta_value' => 'Yes'), array('lead_id' => $entryId, 'meta_key' => 'isApproved'));

        removeFile('/tmp/data-'.$order_id);
        removeFile('/tmp/transaction-'.$order_id);
    }
    $status = array(
        'state'         => $response->response->data->Transaction->state,
        'order_id'      => $order_id,
        'return_url'    => $return_url,
    );

    $response = 'paay.api.awaitingApprovalCallback('.json_encode($status).')';

    return $response;
}

function paay_cancelTransactionGFHandler()
{
    $order_id = $_GET['order_id'];
    removeFile('/tmp/data-'.$order_id);
    removeFile('/tmp/transaction-'.$order_id);
}

function removeFile($file)
{
    if(file_exists($file)){
        unlink($file);
    }
}

# END WALLET FUNCTIONALITY
