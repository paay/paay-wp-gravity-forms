jQuery(document).ready(function(){
    jQuery('#paay-modal').paaymodal();
});

+function ($) {
    'use strict';

    $.fn.paaymodal = function(options)
    {
        var $this = $(this);
        var modal = new PAAYModal($this, options);
        modal.show();
    };

    // PAAY MODAL CLASS

    var PAAYModal = function (element, options)
    {
        this.element = element;
        this.options = options;
        this.isShown = false;

        this.dialogClass = '.paay-modal-dialog';
        this.contentClass = '.paay-modal-content';
        this.headerClass = '.paay-modal-header';
        this.iframeId = '#paay_iframe_form';
        this.noPaayClass = 'no_paay_modal';
        this.animateSpeed = 500;

        this.checkAttempts = 0;
        var that = this;
        this.checkIframeReady = function()
        {
            var iframe = $(that.iframeId);
            that.checkAttempts++;

            iframe.onload = function()
            {
                $(that.contentClass).children(that.headerClass).eq(0).remove();
                clearInterval(that.timer);
            };

            if(that.checkAttempts > 4){
                $(that.contentClass).children(that.headerClass).eq(0).remove();
                clearInterval(that.timer);
            }
        };

        this.timer = setInterval(this.checkIframeReady, 1000);
        this.hideOverflows();
    };

    PAAYModal.prototype.show = function()
    {
        var that = this;
        var top = window.innerHeight * 0.1;

        this.isShown = true;
        that.element.show();
        that.element.children(that.dialogClass).animate({
            'top': "+="+top,
        }, that.animateSpeed);
    };

    PAAYModal.prototype.hideOverflows = function()
    {
        var that = this;

        that.element.parents().each(function(){
            $(this).addClass(that.noPaayClass);
        });
    };

}(jQuery);