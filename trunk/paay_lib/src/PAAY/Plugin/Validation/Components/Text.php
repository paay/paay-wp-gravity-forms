<?php

namespace PAAY\Plugin\Validation\Components;

class Text implements ValidatorInterface
{
    public function valid($value)
    {
        if (strlen(strval($value)) > 0){
            return true;
        }
        return false;
    }
}