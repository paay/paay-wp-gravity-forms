<?php

namespace PAAY\Plugin\Client;

use PAAY\Plugin\Exceptions\ErrorApiResponse;
use PAAY\Plugin\Exceptions\EmptyTransactionUrl;
use PAAY\Plugin\Exceptions\UnexpectedApiResponseException;
use PAAY\Plugin\Storage\StorageInterface;

class ApiStandalone
{
    private $urlTemplate = "%s/standalone-transactions%s";
    private $response;
    private $info;

    public function transactionUrl($host, StorageInterface $storage, $strategy = 'modal')
    {
        $url = $this->createUrl($host, $strategy);

        $this->post($url, $storage->data());

        return $this->selectUrlFromResponse($strategy);
    }

    private function selectUrlFromResponse($strategy)
    {
        $response = ($strategy === 'modal') ? json_decode($this->response, true) : $this->info;

        if(!is_array($response)){
            throw new UnexpectedApiResponseException('PAAY Api did not return expect response');
        }

        if(!array_key_exists('url', $response) || !filter_var($response['url'], FILTER_VALIDATE_URL)){
            throw new EmptyTransactionUrl('PAAY Api did not return transaction url');
        }
        
        return $response['url'];
    }

    private function createUrl($host, $strategy)
    {
        $query = ($strategy === 'modal') ? '?modal=true' : '';

        return sprintf($this->urlTemplate, $host, $query);
    }

    private function post($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $this->response = curl_exec($ch);
        $this->info = curl_getinfo($ch);

        if(curl_errno($ch)){
            throw new ErrorApiResponse(curl_error($ch));
        }

        curl_close($ch);
    }
}
