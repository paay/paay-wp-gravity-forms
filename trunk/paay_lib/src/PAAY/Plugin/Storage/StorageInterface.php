<?php

namespace PAAY\Plugin\Storage;

interface StorageInterface
{
    public function completeFillingUp($key, $secret);
    
    public function data();
}