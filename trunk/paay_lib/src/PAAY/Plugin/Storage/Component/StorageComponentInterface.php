<?php

namespace PAAY\Plugin\Storage\Component;

interface StorageComponentInterface
{
    public function fill(array $data);
    
    public function data();
}
