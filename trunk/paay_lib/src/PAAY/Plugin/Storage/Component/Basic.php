<?php

namespace PAAY\Plugin\Storage\Component;

use PAAY\Plugin\Settings\Settings;

class Basic extends StorageComponent implements StorageComponentInterface
{
    protected $key = Settings::BASIC_FIELDS;
}
