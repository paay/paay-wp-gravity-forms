<?php

namespace PAAY\Plugin\Storage\Component;

use PAAY\Plugin\Exceptions\EmptyFieldAttributeException;
use PAAY\Plugin\Settings\Settings;
use PAAY\Plugin\Validation\Validator;

abstract class StorageComponent
{
    protected $key;
    protected $settings = array();
    protected $validator;
    protected $data = array();

    public function __construct()
    {
        $settings = new Settings();
        
        $this->validator = new Validator();
        $this->settings = $settings->settings($this->key);
    }
    
    public function fill(array $data)
    {
        foreach ($this->settings as $field => $attributes) {
            $this->requireValue($data, $field, (bool)$attributes['required']);
            $this->checkType($data, $field, $attributes['type']);

            $this->data[$field] = !array_key_exists($field, $data) ? null : $data[$field];
        }
        
        return true;
    }
    
    public function data()
    {
        return $this->data;
    }

    protected function requireValue(array $data, $key, $require)
    {
        if($require && !array_key_exists($key, $data)){
            throw new EmptyFieldAttributeException("Field '{$key}' is require but argument was not passed");
        }

        if($require && (empty($data[$key]) || strlen($data[$key]) === 0)){
            throw new EmptyFieldAttributeException("Field '{$key}' is require but argument is empty");
        }
    }

    protected function checkType(array $data, $key, $type)
    {
        if(empty($type) || !array_key_exists($key, $data)){
            return;
        }

        if(strlen($data[$key]) > 0){
            return $this->validator->valid($type, $data[$key], $key);
        }
    }
}
