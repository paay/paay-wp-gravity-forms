<?php

namespace PAAY\Plugin\Storage\Component;

use PAAY\Plugin\Settings\Settings;

class ShippingAddress extends StorageComponent implements StorageComponentInterface
{
    protected $key = Settings::SHIPPING_ADDRESS_FIELDS;
}
