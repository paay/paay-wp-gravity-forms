<?php

class Paay_API_Client
{
    private $host = null;
    private $key = null;
    private $secret = null;
    private $client_name = 'PAAY client';

    public function __construct($host, $key, $secret)
    {
        $this->host = rtrim($host, '/');
        $this->key = $key;
        $this->secret = $secret;
    }

    protected function generateSignature($url, $data)
    {
        $arrayToStringVars = array('data', 'query');
        if (false === strstr($data, '\/')) {
            $data = str_replace('/', '\/', $data);
        }

        $dataStr = '';
        foreach ($arrayToStringVars as $var) {
            if (isset(${$var})) {
                if (is_array(${$var})) {
                    $dataStr .= !empty(${$var}) ? json_encode(${$var}) : '';
                } else {
                    $dataStr .= trim(${$var});
                }
            }
        }

        if (trim($dataStr) > 0) {
            $data = json_decode($dataStr);
            $dataStr = json_encode($data);
        }

        $return = md5(trim($url) . $dataStr . trim($this->secret));

        return $return;
    }

    private function get($url, $query = array())
    {
       $signature = $this->generateSignature($url, '');
       $query = (count($query) > 0) ? '?'.http_build_query($query) : '';
       $url = $this->host.'/'.$url.$query;

       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
       curl_setopt($ch, CURLOPT_USERAGENT, $this->client_name);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       $headers = array(
           'Accept: application/vnd.paay.api.v1+json',
           'Paay-Auth-Key: ' . $this->key,
           'Paay-Signature: ' . $signature
       );
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

       $response = curl_exec($ch);
       if ($response === false) {
           throw new \Exception(curl_error($ch));
       }
       $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
       curl_close($ch);

       $result = json_decode($response);

       return $result;
    }

    private function post($url, $data = array())
    {
        $signature_data = (!empty($data)) ? json_encode($data) : '';
        $signature = $this->generateSignature($url, $signature_data);
        $url = $this->host.'/'.$url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->client_name);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $headers = array(
            'Accept: application/vnd.paay.api.v1+json',
            'Paay-Auth-Key: ' . $this->key,
            'Paay-Signature: ' . $signature
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        if ($response === false) {
            throw new \Exception(curl_error($ch));
        }
        // $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $result = json_decode($response);

        return $result;
    }

    public function getCustomerByPhone($phone)
    {
        $response = $this->get('customers.json', array('phone' => $phone));
        if(empty($response->customers[0])){
            throw new \Exception('User does not exist');
        }

        return $response->customers[0];
    }

    public function createTransaction($order_id, \stdClass $customer, array $items, array $shippings, $return_url = '')
    {
        $transaction = array(
            'phone_number' => $customer->Customer->phone,
            'return_url' => (string)$return_url,
            'signature' => (string)$order_id,
            'cart_items' => array(
                'TransactionItem' => array(),
                'ShippingOption' => array()
            )
        );

        foreach ($items as $item) {
            $transaction['cart_items']['TransactionItem'][] = array(
                'description'   => $item['description'],
                'quantity'      => $item['quantity'],
                'unit_price'    => $item['unit_price'],
            );
        }

        //@TODO: Available shipping methods
        if (is_array($customer->Address)) {
            foreach ($customer->Address as $address) {
                $address_id = (string)$address->id;
                // $state_code = (string)$address->state;

                foreach ($shippings as $shipping_method_name => $shipping_method) {
                    $transaction['cart_items']['ShippingOption'][] = array(
                        'address_id' => $address_id,
                        'name' => $shipping_method_name,
                        'cost' => $shipping_method['price'],
                        'tax' => round($shipping_method['price'] * ($shipping_method['tax_percent'] / 100), 2),
                    );
                }
            }
        }

        $transaction['cart_items'] = base64_encode(json_encode($transaction['cart_items']));

        return $this->post('transactions.json', $transaction);
    }

    public function createApprovedTransaction($request)
    {
        if($request->isRecurring()){
            $uri = 'recurring-payments.json';
        } else {
            $uri = 'transactions.json';
        }
        return $this->post($uri, $request->getPayload());
    }

    public function getTransactionStatus($transaction_id)
    {
        return $this->get('transactions/'.$transaction_id.'.json');
    }

    public function sendWebAppLink($transaction_id, $telephone)
    {
        return $this->post(
            'transactions/send-webapp/'.$transaction_id.'.json',
            array(
                'phone' => $telephone
            )
        );
    }

    public function merchantApproveTransaction($transactionId)
    {
        return $this->post('transactions/merchant-approve/'.$transactionId.'.json');
    }
}
