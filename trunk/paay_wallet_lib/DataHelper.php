<?php

class DataHelper
{
    private $feedData;

    /**
     * Extract id of mapped fields (total)
     * @param array $data
     * @return $this
     * @throws Exception
     */
    public function setFeedData($data)
    {
        if(empty($data)){
            throw new \Exception('Gravity Form PAAY plugin has no set parameters - no mapped fields.');
        }

        $meta = (isset($data['meta'])) ? $data['meta'] : array();
        if(empty($meta)){
            throw new \Exception('Gravity Form PAAY plugin has incorrect settings');
        }

        unset($meta['feedName']);
        unset($meta['transactionType']);

        $this->feedData = array_flip(array_map(function($key){
            return preg_replace('/^billingInformation\_/','',$key);
        }, array_flip($meta)));

        return $this;
    }

    /**
     * Save information about form and form fields (type, id etc.)
     * @param array $data
     * @return $this
     * @throws Exception
     */
    public function setForm($data)
    {
        if(empty($data)){
            throw new \Exception('Gravity form data is empty');
        }
        if(empty($data['fields'])){
            throw new \Exception('Gravity form has no fields');
        }

        $this->form = $data;
        foreach($data['fields'] as $key => $value){
            $this->formFields[] = $value;
        }

        return $this;
    }

    /**
     * Extract form fields from post data
     * @param array $data
     * @return $this
     */
    public function setPost($data)
    {
        foreach($data as $key => $value){
            if(!preg_match('/^input_/', $key)){
                continue;
            }
            $key = preg_replace('/^input_/', '', $key);
            $this->post[$key] = $value;
        }

        return $this;
    }

    /**
     * Return total value for PAAY Button
     * @return double
     * @throws Exception
     */
    public function getTotalFromPost()
    {
        $totalId = $this->findIdTotalField($this->feedData['amount'], $this->formFields);

        if(empty($this->post[$totalId])){
            throw new \Exception('There is no field with amount insiade POST array');
        }

        return $this->post[$totalId];
    }

    private function findIdTotalField($type, $fields)
    {
        foreach($fields as $field){
            if(trim($field['type']) === trim($type)){
                return $field['id'];
            }
        }

        throw new \Exception('There is no field with amount');
    }
}
